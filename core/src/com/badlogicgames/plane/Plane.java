package com.badlogicgames.plane;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by damviod on 22/04/16.
 */
public class Plane
{
    private static final float PLANE_JUMP_IMPULSE = 350;

    private static final float PLANE_VELOCITY_X = 200;
    private static final float PLANE_START_Y = 240;
    private static final float PLANE_START_X = 50;

    private Animation anim;

    private Vector2 planePosition;
    private Vector2 planeVelocity;

    float planeStateTime;

    public boolean isInvencible, drawFrame;
    public int invCounter;

    public boolean fire;

    public Plane()
    {
        planeStateTime = 0;

        planePosition = new Vector2();
        planeVelocity = new Vector2();

        Texture frame1 = new Texture("plane1.png");
        frame1.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Texture frame2 = new Texture("plane2.png");
        Texture frame3 = new Texture("plane3.png");

        anim = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3), new TextureRegion(frame2));
        anim.setPlayMode(Animation.PlayMode.LOOP);

        isInvencible = false;
        invCounter = 3;

        drawFrame = true;

        fire = false;
    }

    public void Reset()
    {
        planePosition.set(PLANE_START_X, PLANE_START_Y);
        planeVelocity.set(0, 0);
    }

    public void SetVelocity()
    {
        planeVelocity.set(PLANE_VELOCITY_X, PLANE_JUMP_IMPULSE);
    }

    public void AddVel(Vector2 gravity)
    {
        planeVelocity.add(gravity);
    }

    public void AddToPos(float deltaTime)
    {
        planePosition.mulAdd(planeVelocity, deltaTime);
    }

    public Vector2 GetPos(){
        return  planePosition;
    }

    public Vector2 GetVel(){
        return  planeVelocity;
    }

    public Animation GetAnim()
    {
        return anim;
    }
}
