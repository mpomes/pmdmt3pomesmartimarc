package com.badlogicgames.plane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

/**
 * Created by damviod on 22/04/16.
 */


public class MenuScreen implements Screen {
    final MainGame game;
    OrthographicCamera camera;

    Music music;

    Texture background;

    public MenuScreen(final MainGame _game) {
        game = _game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        background = new Texture("menuBG.png");

        music = Gdx.audio.newMusic(Gdx.files.internal("PLP_Synths_Demo_03.mp3"));
        music.setLooping(true);
        music.play();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.batch.draw(background, camera.position.x - background.getWidth() / 2, 0);
        game.font.setColor(Color.BLACK);
        game.font.draw(game.batch, "Welcome to PMDM T3 by Pomes Marti Marc", background.getWidth() / 2 - 125, 50);
        game.font.draw(game.batch, "Tap anywhere to begin!", background.getWidth() / 2 - 75, 75);
        game.batch.end();

        if (Gdx.input.isTouched()) {
            game.setScreen(new PlaneGame(game));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        music.dispose();
    }
}

