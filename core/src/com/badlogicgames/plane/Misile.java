package com.badlogicgames.plane;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by damviod on 22/04/16.
 */
public class Misile {

    private static final float MISILE_VELOCITY_X = 200;

    Vector2 misilePosition;
    Vector2 misileVelocity;

    Animation anim;

    float stateTime;

    public Misile(Vector2 posInstance)
    {
        stateTime = 9;

        misilePosition = new Vector2();
        misileVelocity = new Vector2();

        misilePosition.set(posInstance.x + 50, posInstance.y);
        misileVelocity.set(MISILE_VELOCITY_X, 0);

        Texture frame1 = new Texture("missile1.png");
        frame1.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Texture frame2 = new Texture("missile2.png");
        Texture frame3 = new Texture("missile3.png");

        anim = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3), new TextureRegion(frame2));
        anim.setPlayMode(Animation.PlayMode.LOOP);
    }

    public void Update(float deltaTime)
    {
        stateTime += deltaTime;
        misileVelocity.add(10f,0);
        misilePosition.mulAdd(misileVelocity, deltaTime);
        //misilePosition.add(10f * deltaTime,0);
    }
}
